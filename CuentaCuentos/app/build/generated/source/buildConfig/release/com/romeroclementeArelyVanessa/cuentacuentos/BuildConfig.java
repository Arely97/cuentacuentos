/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.romeroclementeArelyVanessa.cuentacuentos;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.romeroclementeArelyVanessa.cuentacuentos";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
