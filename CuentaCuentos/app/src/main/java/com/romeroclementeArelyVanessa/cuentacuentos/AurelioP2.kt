package com.romeroclementeArelyVanessa.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_aurelio_p1.*

class AurelioP2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aurelio_p2)
        siguiente.setOnClickListener {
            val intent= Intent(this,AurelioP3::class.java)
            startActivity(intent)
        }
    }
}