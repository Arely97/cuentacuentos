package com.romeroclementeArelyVanessa.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_aurelio_p1.*
import kotlinx.android.synthetic.main.activity_fin.*

class Fin : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fin)
        btnEnviar.setOnClickListener {
            val intent= Intent(this,Creditos::class.java)
            startActivity(intent)
        }
        Leer.setOnClickListener {
            val intent= Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
    }
}