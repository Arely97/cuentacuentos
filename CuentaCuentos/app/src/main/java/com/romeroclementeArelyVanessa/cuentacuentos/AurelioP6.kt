package com.romeroclementeArelyVanessa.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_aurelio_p1.*

class AurelioP6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aurelio_p6)
        siguiente.setOnClickListener {
            val intent= Intent(this,Fin::class.java)
            startActivity(intent)
        }
    }
}