package com.romeroclementeArelyVanessa.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.ayuda.*
import kotlinx.android.synthetic.main.ayuda.view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {
            val nombres = edtNombre.text.toString()
            if (nombres.isEmpty()) {
                Toast.makeText(this, "Debe ingresar su nombre", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (!chkTerminos.isChecked) {
                Toast.makeText(this, "Debe aceptar los terminos", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            var genero = if (rdbMasculino.isChecked) "Masculino" else "Femenino"



            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres", nombres)
                putString("key_genero", genero)
            }

            val intent=Intent(this,Menu::class.java).apply {
            putExtras(bundle)
            }


            startActivity(intent)

    }
        ayudaanimation.setOnClickListener {
            val bottomSheet = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
            val view = LayoutInflater.from(this).inflate(R.layout.ayuda, contenedor)
            view.dato.text = "Recuerda son cuentos educativos. Algunos aun no estan disponibles."
            bottomSheet.setContentView(view)
            bottomSheet.show()
        }
    }
}