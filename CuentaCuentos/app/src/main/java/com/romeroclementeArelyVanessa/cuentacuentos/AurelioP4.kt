package com.romeroclementeArelyVanessa.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_aurelio_p1.*

class AurelioP4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aurelio_p4)
        siguiente.setOnClickListener {
            val intent= Intent(this,AurelioP5::class.java)
            startActivity(intent)
        }
    }
}