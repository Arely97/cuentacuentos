package com.romeroclementeArelyVanessa.cuentacuentos


import android.content.Intent
import android.os.Bundle

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_menu.*


class Menu : AppCompatActivity() {
    /************************************************************************/
    val language = arrayOf<String>(
        "El Murcielago Aurelio",
        "El Elefante con Gafas",
        "Un Rey muy Impaciente",
        "Lolo, El Niño Lloron",
        "El Muñeco de Nieve",
        "Una Orquesta en el Trastero",
        "Los Pollitos van a la Playa",
        "La ballena Malena se Va de Verbena",
        "Ceci, el Zapatero Peresoso",
        "Hachita, la Traviesa Letrita",
        "El Joyero Generoso"
    )
    val imageId = arrayOf<Int>(
        R.drawable.un,
        R.drawable.zhgfrj,
        R.drawable.zjzjm,
        R.drawable.zjfdj,
        R.drawable.zhrfh,
        R.drawable.zhfh,
        R.drawable.zxv,
        R.drawable.zxd,
        R.drawable.zhfhf,
        R.drawable.zxg,
        R.drawable.zxc
    )


    /***********************************************************************/


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val bundle: Bundle? = intent.extras
        bundle?.let {
                bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombres", "Desconocido")
            val genero = bundleLibriDeNull.getString("key_genero", "")
            usuario.text = nombres
            var sexo = genero
            if (sexo == "Femenino") {
                icono.setImageResource(R.drawable.mujer)
            } else {
                icono.setImageResource(R.drawable.hombre)
            }

        }

        val myListAdapter = MyListAdapter(this, language, imageId)
        listView.adapter = myListAdapter
        /***********************************************************************/
        listView.setOnItemClickListener() { adapterView, view, position, id ->
            val itemAtPos = adapterView.getItemAtPosition(position)
            val itemIdAtPos = adapterView.getItemIdAtPosition(position)

            if(position==0){
                val intent= Intent(this,AurelioP1::class.java)
                startActivity(intent)
            }
            else{
                Toast.makeText(
                    this,
                    "El cuento $itemAtPos No esta disponible aun.",
                    Toast.LENGTH_LONG
                ).show()
            }


        }
        /***********************************************************************/
    }
}
